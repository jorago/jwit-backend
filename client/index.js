const { io } = require('socket.io-client');

const socket = io("http://localhost:8080");

async function  main(){
    try {
     
        setTimeout(() => console.log(socket.id), 500);

        socket.on('res:microservice:create', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:create', { statusCode, data, message });

        })

        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:delete', { statusCode, data, message });

        })

        socket.on('res:microservice:update', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:update', { statusCode, data, message });

        })

        socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:findOne', { statusCode, data, message });

        })

        socket.on('res:microservice:view', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:view', { statusCode, data, message });

        })

        // setInterval(() => socket.emit('req:microservice:view', ({})), 350);

        setTimeout(() => socket.emit('req:microservice:create', ({ name:'Jose', age: 41, color:'verde'})));

        // setTimeout(() => socket.emit('req:microservice:delete', ({ id: 6 })));

        // setTimeout(() => socket.emit('req:microservice:update', ({ name: 'Juan David', id: 1})));

        // setTimeout(() => socket.emit('req:microservice:findOne', ({ id: 5 })));

        // setTimeout(() => socket.emit('req:microservice:view', ({ })));


    } catch (error) {
        console.log(error);
    }
}

main()