import React, { useEffect, useState } from 'react';

const io = require('socket.io-client');

function App() {
    
    const [id, setId] = useState();
    const [data, setData] = useState([]);

    
    useEffect(() => {
        
        const socket = io("http://localhost:8080", { transports: ['websocket'], jsonp: false });
            
        setTimeout(() => setId(socket.id), 500);

        socket.on('res:microservice:create', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:create', { statusCode, data, message });

        })

        socket.on('res:microservice:delete', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:delete', { statusCode, data, message });

        })

        socket.on('res:microservice:update', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:update', { statusCode, data, message });

        })

        socket.on('res:microservice:findOne', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:findOne', { statusCode, data, message });

        })

        socket.on('res:microservice:view', ({ statusCode, data, message }) => {
            
            console.log('res:microservice:view', { statusCode, data, message });
            if (statusCode === 200) setData(data);

        })

        // setInterval(() => socket.emit('req:microservice:view', ({})), 1500);
        setTimeout(() => socket.emit('req:microservice:view', ({ })));


    },[])

    return (
        <div>
            <p>{id ? `Estas en linea ${id} `: 'Fuera de linea'}</p>

            {data.map((item, index) => <p key={index}>Nombre: { item.name } Edad: { item.age } Worker: { item.worker }</p> )}
        </div>
    );
}

export default App;
