const bull = require('bull');

const redis = { host: '192.168.1.95', port: 6379 };

const options = { redis: { host: redis.host, port: redis.port  }};

const queueCreate = bull("curso:create", options);

const queueDelete = bull("curso:delete", options);

const queueUpdate = bull("curso:update", options);

const queueFindOne = bull("curso:findOne", options);

const queueView = bull("curso:view", options);

async function Create({ name, age, color }) {
    try {
        
        const job = await queueCreate.add({ name, age, color })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }) {
    try {
        
        const job = await queueDelete.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ name, age, color, id }) {
    try {
        
        const job = await queueUpdate.add({ name, age, color, id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({ id }) {
    try {
        
        const job = await queueFindOne.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({ }) {
    try {
        console.log('ejecutando view')
        
        const job = await queueView.add({})

        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message }

        } catch (error) {
            console.log(error);
    }
}

module.exports = { Create, Delete, Update, FindOne, View };
