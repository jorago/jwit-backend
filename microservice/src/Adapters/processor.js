const Services = require('../Services');
const { InternalError } = require('../settings');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView } = require('./index');

async function Create(job, done){ 
    try {

        const { name, age, color } = job.data;

        let { statusCode, data, message } = await Services.Create({ name, age, color });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueCreate', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Delete(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueDelete', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Update(job, done){ 
    try {

        const { name, age, color, id } = job.data;

        let { statusCode, data, message } = await Services.Update({ name, age, color, id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueUpdate', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function FindOne(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueFindOne', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function View(job, done){ 
    try {
        const {  } = job.data;

        let { statusCode, data, message } = await Services.View({});
        
        done(null, { statusCode, data: data.map(v => ({ ...v.toJSON(), worker : job.id } )), message });
        
    } catch (error) {
        console.log({step: 'adapter queueView', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}


async function run(){
    try {

        console.log('Iniciando Worker!!')
     
        queueCreate.process(Create)

        queueDelete.process(Delete)

        queueUpdate.process(Update)

        queueFindOne.process(FindOne)

        queueView.process(View)

    } catch (error) {
        console.log(error);
    }
}

module.exports = { Create, Delete, Update, FindOne, View, run }