const Services = require('../Services');
const { InternalError } = require('../settings');

const { queueCreate, queueDelete, queueUpdate, queueFindOne, queueView, queueEnable, queueDisable } = require('./index');

async function Create(job, done){ 
    try {

        const { name, phone } = job.data;

        let { statusCode, data, message } = await Services.Create({ name, phone });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueCreate', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Delete(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Delete({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueDelete', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Update(job, done){ 
    try {

        const { name, phone, email, age, id } = job.data;

        let { statusCode, data, message } = await Services.Update({ name, phone, email, age, id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueUpdate', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function FindOne(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.FindOne({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueFindOne', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function View(job, done){ 
    try {
        const { enable } = job.data;

        let { statusCode, data, message } = await Services.View({ enable });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueView', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Enable(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Enable({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueEnable', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}

async function Disable(job, done){ 
    try {

        const { id } = job.data;

        let { statusCode, data, message } = await Services.Disable({ id });
        
        done(null, { statusCode, data, message });
        
    } catch (error) {
        console.log({step: 'adapter queueDisable', error: error.toString()} );

        done(null, { statusCode: 500, message: InternalError});

    }
}



async function run(){
    try {

        console.log('Iniciando Worker!!')
     
        queueCreate.process(Create)

        queueDelete.process(Delete)

        queueUpdate.process(Update)

        queueFindOne.process(FindOne)

        queueView.process(View)

        queueEnable.process(Enable)

        queueDisable.process(Disable)

    } catch (error) {
        console.log(error);
    }
}

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable, run }