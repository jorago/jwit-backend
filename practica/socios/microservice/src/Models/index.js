const { sequelize, name } = require('../settings');
const { DataTypes } = require('sequelize');

const Model = sequelize.define(name, {
    
    name: { type: DataTypes.STRING },

    age: { type: DataTypes.BIGINT },

    email: { type: DataTypes.STRING },

    phone: { type: DataTypes.STRING },

    enable: { type: DataTypes.BOOLEAN, defaultValue: true },
    
}, { freezeTableName: true});

async function SyncDB(){
    try {

        await Model.sync({logging: false, force: true});
        
        return { statusCode: 200, data: 'Ok' };

    } catch (error) {
        
        console.log(error)
        return { statusCode: 500, message: error.toString() };
    }
}

module.exports = { SyncDB, Model }