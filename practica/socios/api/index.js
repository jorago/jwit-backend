const bull = require('bull');

const { name } = require('./package.json');

const redis = { host: '192.168.1.93', port: 6379 };

const options = { redis: { host: redis.host, port: redis.port  }};

const queueCreate = bull(`${name}:create`, options);

const queueDelete = bull(`${name}:delete`, options);

const queueUpdate = bull(`${name}:update`, options);

const queueFindOne = bull(`${name}:findOne`, options);

const queueView = bull(`${name}:view`, options);

const queueEnable = bull(`${name}:enable`, options);

const queueDisable = bull(`${name}:disable`, options);

async function Create({ name, phone }) {
    try {
        
        const job = await queueCreate.add({ name, phone })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }) {
    try {
        
        const job = await queueDelete.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ name, phone, email, age, id }) {
    try {
        
        const job = await queueUpdate.add({ name, phone, email, age, id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({ id }) {
    try {
        
        const job = await queueFindOne.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({ enable }) {
    try {
        console.log('ejecutando view')
        
        const job = await queueView.add({ enable })

        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message }

        } catch (error) {
            console.log(error);
    }
}

async function Enable({ id }) {
    try {
        
        const job = await queueEnable.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Disable({ id }) {
    try {
        
        const job = await queueDisable.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable };
