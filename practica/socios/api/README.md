# API para el manejo de los Socios

Esta es una api para interactuar con los socios registrados en la biblioteca

## Instalacion

En el directorio de la apicacion ejecutar:

### `npm i api-socio-1.0.0`

## Importar 

```js 
    const apiSocios = require("api-socio");
```

## Funciones Disponibles

### Crear Socio 

```js 

    socket.on('req:socios:create', async ({ name, phone }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.Create({ name, phone });

            return io.to(socket.id).emit('res:socios:create', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
``` 

### Eliminar Socio 

```js 
    socket.on('req:socios:delete', async ({ id }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.Delete({ id });

            return io.to(socket.id).emit('res:socios:delete', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
```

### Actualizar Socio 

```js 
    socket.on('req:socios:update', async ({ name, phone, email, age, id }) => {

        
        try {
            
            const { statusCode, data, message } = await apiSocios.Update({ name, phone, email, age, id });

            return io.to(socket.id).emit('res:socios:update', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
```

### Buscar Socio por id 

```js 
    socket.on('req:socios:findOne', async({ id }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.FindOne({ id });

            return io.to(socket.id).emit('res:socios:findOne', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
```

### Listar Socios por estado 

```js 
    socket.on('req:socios:view', async({ enable }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.View({ enable });

            return io.to(socket.id).emit('res:socios:view', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
```

### Activar Socio 

```js 
    socket.on('req:socios:enable', async({ id }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.Enable({ id });

            return io.to(socket.id).emit('res:socios:enable', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });
```

### Deshabilitar Socio 

```js 
    socket.on('req:socios:disable', async({ id }) => {

        try {
            
            const { statusCode, data, message } = await apiSocios.Disable({ id });

            return io.to(socket.id).emit('res:socios:disable', { statusCode, data, message });

        } catch (error) {
            console.log(error);
        }
    });

```