const bull = require('bull');

const redis = { host: '192.168.1.93', port: 6379 };

const { name } = require('./package.json');

const options = { redis: { host: redis.host, port: redis.port  }};

const queueCreate = bull(`${name}:create`, options);

const queueDelete = bull(`${name}:delete`, options);

const queueUpdate = bull(`${name}:update`, options);

const queueFindOne = bull(`${name}:findOne`, options);

const queueView = bull(`${name}:view`, options);

async function Create({ title, image }) {
    try {
        
        const job = await queueCreate.add({ title, image })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }) {
    try {
        
        const job = await queueDelete.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Update({ title, category, section, image, id }) {
    try {
        
        const job = await queueUpdate.add({ title, category, section, image, id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({ title }) {
    try {
        
        const job = await queueFindOne.add({ title })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({ }) {
    try {
        console.log('ejecutando view')
        
        const job = await queueView.add({})

        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message }

        } catch (error) {
            console.log(error);
    }
}

module.exports = { Create, Delete, Update, FindOne, View };
