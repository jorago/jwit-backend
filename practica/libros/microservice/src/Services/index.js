const Controllers = require('../Controllers');
const { InternalError } = require('../settings');

async function Create({ title, image }) {
    
    try {

        let { statusCode, data, message } = await Controllers.Create({ title, image });

        return { statusCode, data, message } 
        
    } catch (error) {
        console.log({step: 'service Create', error: error.toString()} );
        
        return { statusCode: 500, message: error.toString()};
    }
}

async function Delete({ id }) {
    
    try {

        const findOne = await Controllers.FindOne({ where: { id } });

        if(findOne.statusCode !== 200) {

            switch (findOne.statusCode) {
                case 400: return { statusCode: 400, message: 'No existe el libro a eliminar' }
                
                default: return { statusCode: 500, message: InternalError }
            
            }
        }
        
        let del = await Controllers.Delete({ where: { id } });
        
        if (del.statusCode === 200) return { statusCode: 200, data: findOne.data, message: 'Libro eliminado!!!' };

        return { statusCode:500, message: InternalError } 

    } catch (error) {
        console.log({step: 'service Delete', error: error.toString()} );

        return { statusCode: 500, message: error.toString()};
    }
}

async function Update({ title, category, section, image, id }) {
    
    try {

        let { statusCode, data, message } = await Controllers.Update({ title, category, section, image, id } );
        
        return { statusCode, data, message } 

    } catch (error) {
        console.log({step: 'service Update', error: error.toString()} );

        return { statusCode: 500, message: error.toString()};
    }
}

async function FindOne({ title }) {
    
    try {

        let { statusCode, data, message } = await Controllers.FindOne({ where: { title } });
        
        return { statusCode, data, message } 

    } catch (error) {
        console.log({step: 'service FindOne', error: error.toString()} );

        return { statusCode: 500, message: error.toString()};
    }
}

async function View({}) {
    
    try {

        let { statusCode, data, message } = await Controllers.View({});
        
        return { statusCode, data, message } 

    } catch (error) {
        console.log({step: 'service View', error: error.toString()} );

        return { statusCode: 500, message: error.toString()};
    }
}



module.exports = { Create, Delete, Update, FindOne, View };