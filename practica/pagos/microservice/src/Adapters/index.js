const bull = require('bull');

const { redis, name } = require('../settings');

const options = { redis: { host: redis.host, port: redis.port  }};

const queueCreate = bull(`${name}:create`, options);

const queueDelete = bull(`${name}:delete`, options);

const queueFindOne = bull(`${name}:findOne`, options);

const queueView = bull(`${name}:view`, options);

module.exports = { queueCreate, queueDelete, queueFindOne, queueView };