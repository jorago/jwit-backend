const bull = require('bull');

const redis = { host: '192.168.1.93', port: 6379 };

const { name } = require('./package.json');

const options = { redis: { host: redis.host, port: redis.port  }};

const queueCreate = bull(`${name}:create`, options);

const queueDelete = bull(`${name}:delete`, options);

const queueFindOne = bull(`${name}:findOne`, options);

const queueView = bull(`${name}:view`, options);

async function Create({ socio, amount  }) {
    try {
        
        const job = await queueCreate.add({ socio, amount })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function Delete({ id }) {
    try {
        
        const job = await queueDelete.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function FindOne({ id }) {
    try {
        
        const job = await queueFindOne.add({ id })

        const { statusCode, data, message } = await job.finished();

        return { statusCode, data, message }

    } catch (error) {
        console.log(error);
    }
}

async function View({ }) {
    try {
        console.log('ejecutando view')
        
        const job = await queueView.add({})

        const { statusCode, data, message } = await job.finished();
    
        return { statusCode, data, message }

        } catch (error) {
            console.log(error);
    }
}

module.exports = { Create, Delete, FindOne, View };
