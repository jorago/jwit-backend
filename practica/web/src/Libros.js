/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";

import { socket } from './ws';

import styled from "styled-components";


const Container = styled.div`
    position: relative;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-self: center;
    align-content: strech;
    width: 100%;
`;

const Libro = styled.div`
    background-color: #303536;
    padding: 10px;
    margin-bottom: 10px;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    position: relative;
`;

const Portada = styled.img`
    width: 200px;
    height: 200px;
`;

const Title = styled.p`
    font-size: 1.3rem;
    color: white;
    text-align: center;
    text-transform: capitalize;
`;

const Button = styled.button`
    background-color: #036e00;
    color: white;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 100%;
    height: 50px;
    margin-bottom: 25px;
    font-size: 1.2rem;
    cursor: pointer;
`;

const Icon = styled.img`
    width: 35px;
    height: 35px;
    position: absolute;
    top: -6px;
    right: -5px;
    cursor: pointer;
`;

const Item = ({ image, title, id }) => {

    const handleDelete = () => socket.emit('req:libros:delete', { id });
    
    const opts = {
        src: "https://img.icons8.com/flat-round/344/delete-sign.png",
        onClick: handleDelete
    }
    return (
            <Libro>
                <Icon {...opts} />
                <Portada src={image} />
                <Title>{title}</Title>
            </Libro>
    )}

const App  = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        
        socket.on('res:libros:view', ({ statusCode, data, message }) => {
            
            // console.log('res:libros:view', { statusCode, data, message });
            if (statusCode === 200) setData(data);

        })

        setTimeout(() => socket.emit('req:libros:view', ({ })), 1000);

    },[])

    const libros = [
        { title: 'Aprendiendo nodejs', image: 'https://nodejs.org/static/images/logo.svg'  },
        { title: 'Aprendiendo react',  image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/47/React.svg/512px-React.svg.png' },
        { title: 'Aprendiendo laravel', image: 'https://upload.wikimedia.org/wikipedia/commons/9/9a/Laravel.svg' },
        { title: 'Aprendiendo angular', image: 'https://cdn.worldvectorlogo.com/logos/angular-icon.svg' }
    ];
    
    var rand = Math.floor(Math.random()*libros.length);

    const handleCreate = () => socket.emit('req:libros:create', libros[rand] );

    return (
        <Container>
            <Button onClick={handleCreate}>Create</Button>
            { data.map((v, i) => <Item key={i} {...v} /> ) }
        </Container>
    )
}

export default App;