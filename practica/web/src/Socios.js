/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";

import { socket } from './ws';

import styled from "styled-components";

const Container = styled.div`
    width: 90%;
    max-width: 550px;
`;
const ContainerBody = styled.div`
    height: 400px;
    overflow: scroll;
`;

const Socio = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
`;

const Body = styled.div`
    padding-left: 15px;
    padding-right: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: stretch;
`;

const Name = styled.p`
    color: #333;
`;
const Phone = styled.p`

`;
const Email = styled.p`

`;
const Enable = styled.div`
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: ${props => props.enable ? 'green' : 'red'};
    opacity: 0.6;
    margin-bottom: 10px;
    cursor: pointer;
`;

const Button = styled.button`
    background-color: #f44336;
    color: white;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 90%;
    height: 50px;
    margin-bottom: 25px;
    font-size: 1.2rem;
    cursor: pointer;
`;

const Actions = styled.div`
    display: flex;
    justify-content: flex-end;
`;

const IconUser = styled.img`
    width: 18px;
    height: 18px;
    position: relative;
    cursor: pointer;
    margin-bottom: -2px;
    margin-right: 3px;
`;

const IconPhone = styled.img`
    width: 20px;
    height: 20px;
    position: relative;
    cursor: pointer;
    margin-bottom: -2px;
    margin-right: 3px;
`;

const Icon = styled.img`
    width: 30px;
    height: 30px;
    position: relative;
    cursor: pointer;
    margin-left: 10px;
`;

const App = () => {

    const [data, setData] = useState([]);

    useEffect(() => {
        
        socket.on('res:socios:view', ({ statusCode, data, message }) => {
            
            if (statusCode === 200) setData(data);

        })

        setTimeout(() => socket.emit('req:socios:view', ({ })), 1000);

    },[])


    const socios = [
        { name: 'Juan', phone: '252627'  },
        { name: 'Dario',  phone: '323334' },
        { name: 'Alberto', phone:'251436'  },
        { name: 'Marcos', phone: '225588' }
    ];
    
    var rand = Math.floor(Math.random()*socios.length);

    const handleCreate = () => socket.emit('req:socios:create', socios[rand] );

    const handleDelete = (id) => socket.emit('req:socios:delete', { id });
    
    const handleChangeStatus = (id, status) => {
        console.log('status: ', status);
        if (status) socket.emit('req:socios:disable', { id })
        
        else socket.emit('req:socios:enable', { id })
    }

    return (
        <Container>
        
            <Button onClick={handleCreate}>Create</Button>
            <ContainerBody>
                {
                    data.map((v, i) => { 
                        return (
                            <Socio key={i}> 
                                
                                <Body>

                                    <Name>
                                        <IconUser src={"https://icons-for-free.com/iconfiles/png/512/customer+person+profile+user+icon-1320184293316929121.png"}></IconUser>
                                        {v.name} <small>{v.id}</small>
                                    </Name>

                                    <Phone>
                                        <IconPhone src={"https://icons-for-free.com/iconfiles/png/512/phone+iphone+48px-131985226151637715.png"}></IconPhone>
                                        {v.phone}
                                    </Phone>
                                </Body>
                                <Body>
                                    <Email>{v.email}</Email>

                                    <Actions>
                                        <Enable enable={v.enable} onClick={()=> handleChangeStatus(v.id, v.enable)}/>
                                        <Icon src ={"https://img.icons8.com/flat-round/344/delete-sign.png"} onClick={() => handleDelete(v.id)} />
                                    </Actions>

                                </Body>
                            </Socio> 
                        )
                    })
                }

            </ContainerBody>
        </Container>

    )
}

export default App;