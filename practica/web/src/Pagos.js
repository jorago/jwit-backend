/* eslint-disable array-callback-return */
import React, { useEffect, useState } from "react";

import { socket } from './ws';

import styled from "styled-components";

const Container = styled.div`
    width: 90%;
    max-width: 550px;
`;
const ContainerBody = styled.div`
    height: 550px;
    overflow: scroll;
`;

const Pago = styled.div`
    display: flex;
    flex-direction: column;
    margin-bottom: 15px;
`;

const Body = styled.div`
    padding-left: 15px;
    padding-right: 15px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: stretch;
`;

const Name = styled.p`
    color: #333;
`;

const Button = styled.button`
    background-color: #f44336;
    color: white;
    padding: 10px 20px 10px 20px;
    border-radius: 15px;
    opacity: 0.8;
    width: 90%;
    height: 50px;
    margin-bottom: 25px;
    font-size: 1.2rem;
    cursor: pointer;
`;

const Actions = styled.div`
    display: flex;
    justify-content: flex-end;
`;

const Icon = styled.img`
    width: 30px;
    height: 30px;
    position: relative;
    cursor: pointer;
    margin-left: 10px;
`;

const IconUser = styled.img`
    width: 18px;
    height: 18px;
    position: relative;
    cursor: pointer;
    margin-bottom: -2px;
    margin-right: 3px;
`;

const IconCash = styled.img`
    width: 18px;
    height: 18px;
    position: relative;
    cursor: pointer;
    margin-bottom: -2px;
    margin-right: 3px;
`;

const Input = styled.input`
    width: 90%;
    height: 25px;
    border-radius: 2px;
    position: relative;
    cursor: pointer;
    margin-left: 10px;
    margin-top: 10px;
    margin-bottom: 10px;
    text-align: right;
    padding-right: 10px;
`;

const App = () => {

    const [data, setData] = useState([]);
    const [value, setValue] = useState([]);

    useEffect(() => {
        
        socket.on('res:pagos:view', ({ statusCode, data, message }) => {
            
            if (statusCode === 200) setData(data);

        })

        setTimeout(() => socket.emit('req:pagos:view', ({ })), 1000);

    },[])

    const handleCreate = () => value > 0 ?  socket.emit('req:pagos:create', { socio: value, amount: 300 } ) : null;

    const handleDelete = (id) => socket.emit('req:pagos:delete', { id });
    
    const handleInput = (e) => setValue(e.target.value)

    return (
        <Container>
            
            <Input type="number" onChange={handleInput} ></Input>
            
            <Button onClick={handleCreate}>Aplicar pago para el socio {value}</Button>

            <ContainerBody>
                {
                    data.map((v, i) => { 
                        return (
                            <Pago key={i}> 
                                <Body>
                                    <Name>
                                        <IconCash src={"https://icons-for-free.com/iconfiles/png/512/money-1319964824913825503.png"}></IconCash>
                                        Cupon de pago {v.id}
                                    </Name>

                                    <Name>
                                        <IconUser src={"https://icons-for-free.com/iconfiles/png/512/customer+person+profile+user+icon-1320184293316929121.png"}></IconUser>
                                        Socio {v.socio}
                                    </Name>

                                </Body>
                                <Body>


                                    <Name><small>{ v.createdAt }</small></Name>

                                    <Actions>
                                        <Icon src ={"https://img.icons8.com/flat-round/344/delete-sign.png"} onClick={() => handleDelete(v.id)} />
                                    </Actions>

                                </Body>
                            </Pago> 
                        )
                    })
                }

            </ContainerBody>
        </Container>

    )
}

export default App;