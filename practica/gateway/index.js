const express = require('express');
const http = require('http');

const app = express();

const server = http.createServer(app);

const { Server } = require('socket.io');

const io = new Server(server);

const apiSocios = require("socios");
const apiLibros = require("libros");
const apiPagos = require("pagos");

server.listen(8080, () => {
    console.log('Server initialize');

    io.on('connection', socket => {
        
        console.log('New connection', socket.id);

        // Socios
        socket.on('req:socios:create', async ({ name, phone }) => {

            try {
                
                console.log('req:socios:create', ({ name, phone }));
    
                const { statusCode, data, message } = await apiSocios.Create({ name, phone });
    
                io.to(socket.id).emit('res:socios:create', { statusCode, data, message });

                const view = await apiSocios.View({ });
    
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:delete', async ({ id }) => {

            try {
                console.log('req:socios:delete', ({ id }));
                
                const { statusCode, data, message } = await apiSocios.Delete({ id });
    
                io.to(socket.id).emit('res:socios:delete', { statusCode, data, message });
                
                const view = await apiSocios.View({ });
    
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:update', async ({ name, phone, email, age, id }) => {

            
            try {
                console.log('req:socios:update', ({ name, phone, email, age, id }));
                
                const { statusCode, data, message } = await apiSocios.Update({ name, phone, email, age, id });
    
                return io.to(socket.id).emit('res:socios:update', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
    


        });

        socket.on('req:socios:findOne', async({ id }) => {

            try {
                console.log('req:socios:findOne', ({ id }));
                
                const { statusCode, data, message } = await apiSocios.FindOne({ id });
    
                return io.to(socket.id).emit('res:socios:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:view', async({ }) => {

            try {
                
                const { statusCode, data, message } = await apiSocios.View({  });
    
                return io.to(socket.id).emit('res:socios:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:enable', async({ id }) => {

            try {

                const { statusCode, data, message } = await apiSocios.Enable({ id });
    
                io.to(socket.id).emit('res:socios:enable', { statusCode, data, message });

                const view = await apiSocios.View({ });
    
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:socios:disable', async({ id }) => {

            try {
                
                const { statusCode, data, message } = await apiSocios.Disable({ id });
    
                io.to(socket.id).emit('res:socios:disable', { statusCode, data, message });

                const view = await apiSocios.View({ });
    
                return io.to(socket.id).emit('res:socios:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        // Libros
        socket.on('req:libros:create', async ({ title, image }) => {

            try {
                
                console.log('req:libros:create', ({ title, image }));
    
                const { statusCode, data, message } = await apiLibros.Create({ title, image });
    
                io.to(socket.id).emit('res:libros:create', { statusCode, data, message });
                
                const view = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view', view);

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:delete', async ({ id }) => {

            try {
                console.log('req:libros:delete', ({ id }));
                
                const { statusCode, data, message } = await apiLibros.Delete({ id });
                

                io.to(socket.id).emit('res:libros:delete', { statusCode, data, message });

                const view = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view', view);
    

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:update', async ({ title, category, section, id }) => {

            
            try {
                console.log('req:libros:update', ({ title, category, section, id }));
                
                const { statusCode, data, message } = await apiLibros.Update({ title, category, section, id });
    
                return io.to(socket.id).emit('res:libros:update', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
    


        });

        socket.on('req:libros:findOne', async({ id }) => {

            try {
                console.log('req:libros:findOne', ({ id }));
                
                const { statusCode, data, message } = await apiLibros.FindOne({ id });
    
                return io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:libros:view', async({}) => {

            try {
                
                console.log('req:libros:view');

                const { statusCode, data, message } = await apiLibros.View({  });
    
                return io.to(socket.id).emit('res:libros:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        // Pagos
        socket.on('req:pagos:create', async ({ socio, amount }) => {

            try {
                
                console.log('req:pagos:create', ({ socio, amount }));
    
                const { statusCode, data, message } = await apiPagos.Create({ socio, amount });
    
                io.to(socket.id).emit('res:pagos:create', { statusCode, data, message });

                const view = await apiPagos.View({ });
    
                return io.to(socket.id).emit('res:pagos:view', view );

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:pagos:delete', async ({ id }) => {

            try {
                console.log('req:pagos:delete', ({ id }));
                
                const { statusCode, data, message } = await apiPagos.Delete({ id });
    
                io.to(socket.id).emit('res:pagos:delete', { statusCode, data, message });

                const view = await apiPagos.View({});

                return io.to(socket.id).emit('res:pagos:view', view);

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:pagos:findOne', async({ id }) => {

            try {
                console.log('req:pagos:findOne', ({ id }));
                
                const { statusCode, data, message } = await apiPagos.FindOne({ id });
    
                return io.to(socket.id).emit('res:pagos:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });

        socket.on('req:pagos:view', async({}) => {

            try {
                console.log('req:pagos:view', ({  }));
                
                const { statusCode, data, message } = await apiPagos.View({  });
    
                return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }

        });
        
    });
});
